// main archive responsible to connect all api
var http = require('http')
	, url = require('url') 
	, fs = require('fs')
	, cube = require('./modules/cube')
	, handler = {};

handler["/"] = index;
function index (req ,res) {
	// print home view.
	console.log("home");

  	fs.readFile('./views/index.html', 'utf8', function (err ,data) {
	  	if (err) {
	    	return console.log(err);
	  	}

	  	res.writeHead(200, {"Content-Type": "text/html"});
	  	res.write(data);
	  	res.end();
	});
}

handler["/get-cube"] = get_cube;
function get_cube (req, res) {
	// return the cube and the operations requested. 
	console.log("optains cube");
	res.writeHead(200, {"Content-Type": "application/json"});

	var params = url.parse(req.url,true).query
		, data = JSON.parse(params.d)
		, array = {}
		, r = []

	for (var i in data) {
  		var options = data[i]
  			, myCube = cube.cube(res, options);

  		delete options.dimention;
        
        for (var j in options) {
            if (options[j]["type"] == 1) {
                var c = options[j]["data"];
               
                array = myCube.update(parseInt(c[0]), parseInt(c[1]), parseInt(c[2]), parseInt(c[3]));  

            } else {
	            var c = options[j]["data"]
	                , from = {
	                    "x":parseInt(c[0]),
	                    "y":parseInt(c[1]),
	                    "z":parseInt(c[2]),
	                }
	                , to = {
	                    "x":parseInt(c[3]),
	                    "y":parseInt(c[4]),
	                    "z":parseInt(c[5])
	                };

	            r.push(myCube.query(from, to, array));
            };
        };
        console.log(array);
	}
	
	res.end(JSON.stringify(r));return;
	// test operations
	// res.end(JSON.stringify(myCube.array));
	// myCube.show(myCube.array);
	// myCube.update(2,2,2,4);
	// myCube.update(1,1,1,23);

	// var query = myCube.query({"x":1, "y":1, "z":1},{"x":3, "y":3, "z":3});
	
	// myCube.show(query);
	// myCube.show(myCube.array);
}

function router (handler, pathName ,req , res) {
	// provide a routing system, associate path name to a function
	console.log(pathName);
	if (typeof handler[pathName] === 'function') {
		handler[pathName](req, res);
	} else {
		res.writeHead(404, {"Content-Type": "text/html"});
        res.write("404, " + pathName + "Not Found");
	}
}

var server = http.createServer(function (req, res) {
	var pathName = url.parse(req.url).pathname;
	router(handler, pathName ,req , res); 
}).listen(8080);
// console.log("listen a port 8080");

