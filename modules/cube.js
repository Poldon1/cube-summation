module.exports = {
    // module responsible to create and process the cube
	cube: function (res, options) {
        // initialice the codigo.
        this.res = res;
		this.create(options.dimention);

		return this;
	},
	create: function (dimention) {
        // Create the cube and dimentions
        this.array = {}
   
        var array = {};
        for (var x = 1; x <= dimention; x++) {
            array[x] = {};
            for (var y = 1; y <= dimention; y++) {
                    array[x][y] = {};
                    for (var z = 1; z <= dimention; z++) {
                        array[x][y][z] = 0;
                    }; 
            };
        };

        this.array = array;
	},
    update: function (x, y, z, w) {
        // insert a value in the indicate position
        this.array[x][y][z] = w;
        this.array = this.array;
        return this.array;
    },
    query: function (from, to, array) {
        // return value of summation  
        var find = 0;

        for (var x = from.x; x <= to.x; x++) {
            for (var y = from.y; y <= to.y; y++) {
                for (var z = from.z; z <= to.z; z++) {
                    
                    if (array[x][y][z] != 0) {
                        find = find + array[x][y][z];
                    };
                };
            };
        };

        return find;
    },
    show: function (item) {
        // print array in the view
        this.res.write(JSON.stringify(item));
    }
};